﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Spiroglyphics
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            imgSvg.ImageUrl = "40-dubbelspiraal.svg";
            hlkSvg.Visible = false;
            if (IsPostBack)
            {
                if (tbxKey.Text == "g3heim")
                {
                    if (fulBmp.HasFile) Do_It();
                    else tbxKey.Text = "WTF?";
                }
                else tbxKey.Text = "Password...";
            }
            // TEST TOEKOMST
            //else
            //{
                //string svg = FutureToSvg(40);
                //imgSvg.ImageUrl = "data:image/svg+xml;base64," + Convert.ToBase64String(Encoding.UTF8.GetBytes(svg.Replace("<path fill", "<path xfill")));
            //}

        }

        protected void Do_It()
        {
            Bitmap bmp = new Bitmap(fulBmp.PostedFile.InputStream);
            short orientation = 0;
            try
            {
                orientation = BitConverter.ToInt16(bmp.GetPropertyItem(0x0112).Value, 0);
            } catch { }
            int x, y, w, h;
            if (bmp.Width > bmp.Height)
            {
                x = Convert.ToInt32(Math.Round((bmp.Width - bmp.Height) / 2.0));
                y = 0;
                w = bmp.Height;
                h = bmp.Height;
            }
            else
            {
                x = 0;
                y = Convert.ToInt32(Math.Round((bmp.Height - bmp.Width) / 2.0));
                w = bmp.Width;
                h = bmp.Width;
            }
            //bmp = bmp.Clone(new Rectangle(x, y, w, h), bmp.PixelFormat);
            bmp = new Bitmap(bmp, 702, 702);
            //bmp = Make_Gray(bmp);
            if (orientation == 6) bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            else if (orientation == 8) bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

            string svg = BmpToSvg(bmp);
            bmp.Dispose();
            imgSvg.ImageUrl = "data:image/svg+xml;base64," + Convert.ToBase64String(Encoding.UTF8.GetBytes(svg.Replace("<path fill", "<path xfill")));
            hlkSvg.NavigateUrl = "data:image/svg+xml;base64," + Convert.ToBase64String(Encoding.UTF8.GetBytes(svg));
            hlkSvg.Visible = true;
        }

        public static Bitmap Make_Gray(Bitmap bmp)
        {
            Bitmap newBitmap = new Bitmap(bmp.Width, bmp.Height);
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                ColorMatrix colorMatrix = new ColorMatrix(
                   new float[][]
                   {
                        new float[] {.3f, .3f, .3f, 0, 0},
                        new float[] {.59f, .59f, .59f, 0, 0},
                        new float[] {.11f, .11f, .11f, 0, 0},
                        new float[] {0, 0, 0, 1, 0},
                        new float[] {0, 0, 0, 0, 1}
                   });
                using (ImageAttributes attributes = new ImageAttributes())
                {
                    attributes.SetColorMatrix(colorMatrix);
                    g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attributes);
                }
            }
            return newBitmap;
        }

        protected string BmpToSvg(Bitmap bmp)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            string[] lines = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), imgSvg.ImageUrl));
            List<string> l = new List<string>();
            for (int i = 0; i < lines.Length; i++) // elke lijn in bestand, uitlezen
            {
                string line = lines[i];
                if (line.Trim().StartsWith("C")) l.Add(line.Trim().Substring(1));
            }
            string[] pathd = l.ToArray();
            for (int i = 0; i < pathd.Length / 2; i++) // elk te interpreteren punt (of beter: setje punten met anchors)
            {
                double[] puntA = pathd[i].Split(',').Select(double.Parse).ToArray(); // puntAin en puntA manipuleren (is eerst 'binnen' maar wordt halverwege 'buiten'? moet niet uitmaken)
                double[] puntAuit = pathd[i + 1].Split(',').Select(double.Parse).ToArray(); // puntAuit (is bij de laatste for hetzelfde als puntB)
                double[] puntB = pathd[pathd.Length - (i + 1)].Split(',').Select(double.Parse).ToArray(); // puntBin en puntB
                double[] puntBuit = i == 0 ? puntA : pathd[pathd.Length - i].Split(',').Select(double.Parse).ToArray(); // puntBuit manipuleren (is bij de eerste for hetzelfde als puntA)

                double puntMx = (puntA[4] + puntB[4]) / 2;
                double puntMy = (puntA[5] + puntB[5]) / 2;

                double meting = Pix(bmp, (int)Math.Ceiling(puntMx * 3 + 351), (int)Math.Ceiling(puntMy * 3 + 351));
                Console.WriteLine(meting);
                double factor;
                if (meting <= 38) factor = 1;
                else if (meting >= 217) factor = 0;
                else factor = 1 - (meting - 38) / 179.0;

                double xMove = factor * -3.7 * (puntMx - puntA[4]); // B dan * -1 !!!
                // UITZONDERING in het midden yMove bij uizondering gigantisch vergroten
                if (i == 2611) xMove *= 5;
                double yMove = factor * -3.7 * (puntMy - puntA[5]);

                puntA[2] += xMove;
                puntA[3] += yMove;
                puntA[4] += xMove;
                puntA[5] += yMove;
                if (i == pathd.Length / 2 - 1)
                {
                    puntB[0] += xMove;
                    puntB[1] += yMove;
                }
                else
                {
                    puntAuit[0] += xMove;
                    puntAuit[1] += yMove;
                    pathd[i + 1] = string.Join(",", puntAuit);
                }
                puntB[2] -= xMove;
                puntB[3] -= yMove;
                puntB[4] -= xMove;
                puntB[5] -= yMove;
                if (i == 0)
                {
                    puntA[0] -= xMove;
                    puntA[1] -= yMove;
                }
                else
                {
                    puntBuit[0] -= xMove;
                    puntBuit[1] -= yMove;
                    pathd[pathd.Length - i] = string.Join(",", puntBuit);
                }
                pathd[i] = string.Join(",", puntA);
                pathd[pathd.Length - (i + 1)] = string.Join(",", puntB);

            }

            string sReturn = "";
            for (int i = 0; i < lines.Length; i++) // elke lijn in bestand, uitlezen
            {
                string line = lines[i];
                if (line.Trim().StartsWith("M")) sReturn += Regex.Replace("M" + string.Join(",", pathd[pathd.Length - 1].Split(',').Skip(4).Take(2)) + Environment.NewLine + "C" + string.Join(Environment.NewLine + "C", pathd), @"(\.\d{7})\d+", "$1") + Environment.NewLine;
                else if (line.Trim().StartsWith("C") == false) sReturn += line + Environment.NewLine;
            }
            return sReturn;

        }

        protected double Pix(Bitmap bmp, int x, int y)
        {
            int sum = 0;
            int[,] basepixs = new int[,] {
                { -2, -5 }, { -1, -5 }, { 0, -5 }, { 1, -5 },
                { -4, -4 }, { -3, -4 }, { -2, -4 }, { -1, -4 }, { 0, -4 }, { 1, -4 }, { 2, -4 }, { 3, -4 },
                { -4, -3 }, { -3, -3 }, { -2, -3 }, { -1, -3 }, { 0, -3 }, { 1, -3 }, { 2, -3 }, { 3, -3 },
                { -5, -2 }, { -4, -2 }, { -3, -2 }, { -2, -2 }, { -1, -2 }, { 0, -2 }, { 1, -2 }, { 2, -2 }, { 3, -2 }, { 4, -2 },
                { -5, -1 }, { -4, -1 }, { -3, -1 }, { -2, -1 }, { -1, -1 }, { 0, -1 }, { 1, -1 }, { 2, -1 }, { 3, -1 }, { 4, -1 },
                { -5, 0 }, { -4, 0 }, { -3, 0 }, { -2, 0 }, { -1, 0 }, { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 4, 0 },
                { -5, 1 }, { -4, 1 }, { -3, 1 }, { -2, 1 }, { -1, 1 }, { 0, 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 },
                { -4, 2 }, { -3, 2 }, { -2, 2 }, { -1, 2 }, { 0, 2 }, { 1, 2 }, { 2, 2 }, { 3, 2 },
                { -4, 3 }, { -3, 3 }, { -2, 3 }, { -1, 3 }, { 0, 3 }, { 1, 3 }, { 2, 3 }, { 3, 3 },
                { -2, 4 }, { -1, 4 }, { 0, 4 }, { 1, 4}
            }; 
            for (int i = 0; i < basepixs.GetLength(0); i++)
            {
                Color px = bmp.GetPixel(x + (int)basepixs.GetValue(i, 0), y + (int)basepixs.GetValue(i, 1));
                sum += int.Parse(px.R.ToString());
            }
            return sum / basepixs.GetLength(0);
        }

        /* protected string FutureToSvg(int grooves)
        {
            string sReturn = "<svg version=\"1.2\" baseProfile=\"tiny\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" " +
                "x=\"0px\" y=\"0px\" viewBox=\"-117 -117 234 234\" overflow=\"visible\" xml:space=\"preserve\"><g>" +
                "<path fill=\"none\" stroke=\"#000000\" stroke-width=\"0.0708661\" stroke-miterlimit=\"10\" d=\"";

            // doe iets speciaals met de eerste 'groove' ... nu ff:
            sReturn += "M0,0"; // maar dit wordt dus de 'S'

            for (int i = 1; i < grooves; i++) // elke volgende groove           VOOR NU ga ik alleen de bovenste S-boog ENKEL tekenen, daarna is het een kwestie van alles 4 keer uitvoeren???
            {
                int radius = i + 1;
                int point = (int)Math.Ceiling((i + 1) * Math.PI);
                double angle_step = 180.0 / point;
                double anchor_len = Math.Tan((Math.PI / 180) * (angle_step / 4)) * (4 / 3) * radius;

                for (int j = 0; j < point; j++) // elke punt op de groove ... HEE maar dit is R2Londerlangs, om en om per i dit wisselen  met L2Rbovenlangs???? o en L2Ronderlangs en R2Lbovenlangs!
                {
                    // dus i = even of??
                    double ang_a = (Math.PI / 180) * (0 - j * angle_step);
                    double ang_b = (Math.PI / 180) * (0 - (j + 1) * angle_step);
                    double x = Math.Cos(ang_b) * radius;
                    double y = Math.Sin(ang_b) * radius;
                }
            }


            return sReturn + "/></g></svg>";
        } */
    }
}